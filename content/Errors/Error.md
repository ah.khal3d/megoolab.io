---
title: Errors!
weight: -20
---


<!-- spellchecker-disable -->

{{< toc >}}

<!-- spellchecker-enable -->

## UI Error

One of the erros I faced during running the website is disorientation of elements inside the browser.

[![Example file-tree menu](/media/2.png)](/media/2.PNG)

After searching I found that this line of code on the config.yaml file fixed it

[![Example file-tree menu](/media/1.png)](/media/1.PNG)

## URL Error

The working URL for the website is "https://ah.khal3d.gitlab.io/megoo.gitlab.io/" it is supposed to be "https://megoo.gitlab.io/"; I think it has something to do with authority.

